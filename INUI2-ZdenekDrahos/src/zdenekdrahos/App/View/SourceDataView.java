package zdenekdrahos.App.View;

import java.io.FileNotFoundException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import zdenekdrahos.App.Controller.ISourceDataController;

public class SourceDataView extends javax.swing.JPanel {

    private ISourceDataController controller;

    public SourceDataView() {
        initComponents();        
    }

    public void setController(ISourceDataController controller) {
        this.controller = controller;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        sourceDataJTable = new javax.swing.JTable();
        controlsJPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        filePathTextField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        columnSeparatorTextField = new javax.swing.JTextField();
        loadDataJButton = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        columnsCountJSpinner = new javax.swing.JSpinner();

        setLayout(new java.awt.BorderLayout());

        jScrollPane2.setBackground(new java.awt.Color(250, 250, 250));

        sourceDataJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(sourceDataJTable);

        add(jScrollPane2, java.awt.BorderLayout.CENTER);

        controlsJPanel.setBackground(new java.awt.Color(250, 250, 250));
        controlsJPanel.setPreferredSize(new java.awt.Dimension(409, 105));
        controlsJPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setText("Path to file:");
        controlsJPanel.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 10, -1, -1));

        filePathTextField.setEnabled(false);
        controlsJPanel.add(filePathTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 10, 300, -1));

        jLabel3.setText("Column separator:");
        controlsJPanel.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 42, -1, -1));

        columnSeparatorTextField.setText(";");
        controlsJPanel.add(columnSeparatorTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 40, 70, -1));

        loadDataJButton.setText("Load file");
        loadDataJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadDataJButtonActionPerformed(evt);
            }
        });
        controlsJPanel.add(loadDataJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, 300, -1));

        jLabel4.setText("Number of columns:");
        controlsJPanel.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 42, -1, -1));

        columnsCountJSpinner.setValue(2
        );
        controlsJPanel.add(columnsCountJSpinner, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 40, 60, -1));

        add(controlsJPanel, java.awt.BorderLayout.PAGE_START);
    }// </editor-fold>//GEN-END:initComponents

    private void loadDataJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadDataJButtonActionPerformed
        try {
            String path = getPathToFile();
            int columnsCount = getColumnsCount();
            char columnSeparator = getColumnSeparator();
            filePathTextField.setText(path);
            controller.loadCsvFile(path, columnsCount, columnSeparator);
            setDataToTable();
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(this, "File not found", "File not found", JOptionPane.ERROR_MESSAGE);
        } catch (NullPointerException e) {
            // cancel JFileChooser
        }
    }//GEN-LAST:event_loadDataJButtonActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField columnSeparatorTextField;
    private javax.swing.JSpinner columnsCountJSpinner;
    private javax.swing.JPanel controlsJPanel;
    private javax.swing.JTextField filePathTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton loadDataJButton;
    private javax.swing.JTable sourceDataJTable;
    // End of variables declaration//GEN-END:variables

    private String getPathToFile() {
        JFileChooser fileChooser = new JFileChooser(getCurrentDirectory());
        FileNameExtensionFilter filters = new FileNameExtensionFilter("CSV, csv", "CSV", "csv");
        fileChooser.setFileFilter(filters);
        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            return fileChooser.getSelectedFile().getAbsolutePath();
        }
        throw new NullPointerException();
    }

    private String getCurrentDirectory() {
        return filePathTextField.getText();
    }
    
    private int getColumnsCount() {
        return Integer.parseInt(columnsCountJSpinner.getValue().toString());
    }

    private char getColumnSeparator() {
        return columnSeparatorTextField.getText().charAt(0);
    }

    private void setDataToTable() {
        sourceDataJTable.setModel(controller.getTableModel());
    }
}
