package zdenekdrahos.App.View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import javax.swing.JOptionPane;
import org.jfree.chart.plot.Plot;
import org.jfree.data.xy.XYSeriesCollection;
import zdenekdrahos.AI.Training.Output.TrainingOutput;
import zdenekdrahos.AI.Training.TrainingInput;
import zdenekdrahos.App.Controller.IBackPropagationController;
import zdenekdrahos.App.View.Graphs.IJFreeCharts;
import zdenekdrahos.App.View.Graphs.JFreeCharts;

public class BackPropagationView extends javax.swing.JPanel {

    private IBackPropagationController controller;
    private TrainingInput input = new TrainingInput();
    private Cursor waitCursor = new Cursor(Cursor.WAIT_CURSOR);
    private Cursor defaultCursor = new Cursor(Cursor.DEFAULT_CURSOR);
    private IJFreeCharts graphs = new JFreeCharts();

    public BackPropagationView() {
        initComponents();
    }

    public void setController(IBackPropagationController controller) {
        this.controller = controller;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        controlsJPanel = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        layersJTextField = new javax.swing.JTextField();
        activationsJTextField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        iterationsCountJSlider = new javax.swing.JSlider();
        iterationCountJLabel = new javax.swing.JLabel();
        alfaJTextField = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        momentumJTextField = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        trainJButton = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        targetsJComboBox = new javax.swing.JComboBox();
        inputsJComboBox = new javax.swing.JComboBox();
        jLabel9 = new javax.swing.JLabel();
        errorJTextField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        minErrorJLabel = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        lastErrorJLabel = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        iterationsJLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        graphJPanel = new javax.swing.JPanel();

        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                formComponentShown(evt);
            }
        });
        setLayout(new java.awt.BorderLayout());

        controlsJPanel.setBackground(new java.awt.Color(250, 250, 250));
        controlsJPanel.setPreferredSize(new java.awt.Dimension(400, 230));
        controlsJPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setText("Layers:");
        controlsJPanel.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 10, -1, -1));

        layersJTextField.setText("1 2 1");
        controlsJPanel.add(layersJTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 7, 70, -1));

        activationsJTextField.setText("TANH LIN");
        controlsJPanel.add(activationsJTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 7, 110, -1));

        jLabel3.setText("Activations:");
        controlsJPanel.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(215, 10, -1, -1));

        jLabel4.setText("Number of iterations:");
        controlsJPanel.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 73, -1, -1));

        iterationsCountJSlider.setBackground(new java.awt.Color(250, 250, 250));
        iterationsCountJSlider.setMajorTickSpacing(500);
        iterationsCountJSlider.setMaximum(5000);
        iterationsCountJSlider.setMinorTickSpacing(50);
        iterationsCountJSlider.setPaintLabels(true);
        iterationsCountJSlider.setPaintTicks(true);
        iterationsCountJSlider.setValue(500);
        iterationsCountJSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                iterationsCountJSliderStateChanged(evt);
            }
        });
        controlsJPanel.add(iterationsCountJSlider, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 70, 292, -1));

        iterationCountJLabel.setText("500");
        controlsJPanel.add(iterationCountJLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 80, -1, -1));

        alfaJTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        alfaJTextField.setText("0.3");
        controlsJPanel.add(alfaJTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 36, 30, -1));

        jLabel5.setText("Learning rate:");
        controlsJPanel.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 40, -1, -1));

        jLabel6.setText("Momentum:");
        controlsJPanel.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 40, -1, -1));

        momentumJTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        momentumJTextField.setText("0");
        controlsJPanel.add(momentumJTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 36, 30, -1));

        jLabel7.setForeground(new java.awt.Color(204, 204, 204));
        jLabel7.setText("SIG, TANH, LIN");
        controlsJPanel.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 10, -1, -1));

        jLabel8.setForeground(new java.awt.Color(204, 204, 204));
        jLabel8.setText("<0,1>");
        controlsJPanel.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 40, -1, -1));

        trainJButton.setText("Train network & display results");
        trainJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                trainJButtonActionPerformed(evt);
            }
        });
        controlsJPanel.add(trainJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 160, 360, -1));

        jLabel10.setText("Inputs:");
        controlsJPanel.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(88, 130, -1, -1));

        jLabel11.setText("Targets:");
        controlsJPanel.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 130, -1, -1));

        controlsJPanel.add(targetsJComboBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 126, 150, 20));

        controlsJPanel.add(inputsJComboBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 126, 150, -1));

        jLabel9.setText("Error tolerance:");
        controlsJPanel.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 40, -1, -1));

        errorJTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        errorJTextField.setText("0.005");
        controlsJPanel.add(errorJTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 36, 60, -1));

        jLabel1.setText("MIN ERROR:");
        controlsJPanel.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 200, -1, -1));

        jLabel12.setForeground(new java.awt.Color(204, 204, 204));
        jLabel12.setText("Results:");
        controlsJPanel.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 200, -1, -1));

        minErrorJLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        minErrorJLabel.setText("?");
        controlsJPanel.add(minErrorJLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 200, 60, -1));

        jLabel14.setText("LAST ERROR:");
        controlsJPanel.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 200, -1, -1));

        lastErrorJLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lastErrorJLabel.setText("?");
        controlsJPanel.add(lastErrorJLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 200, 70, -1));

        jLabel16.setText("LAST ITERATION:");
        controlsJPanel.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 200, -1, -1));

        iterationsJLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        iterationsJLabel.setText("?");
        controlsJPanel.add(iterationsJLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 200, 60, -1));
        controlsJPanel.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 190, 510, -1));

        add(controlsJPanel, java.awt.BorderLayout.NORTH);

        graphJPanel.setBackground(new java.awt.Color(255, 255, 255));
        graphJPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        graphJPanel.setForeground(javax.swing.UIManager.getDefaults().getColor("Button.background"));
        graphJPanel.setLayout(new java.awt.BorderLayout());
        add(graphJPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void iterationsCountJSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_iterationsCountJSliderStateChanged
        iterationCountJLabel.setText("" + iterationsCountJSlider.getValue());
    }//GEN-LAST:event_iterationsCountJSliderStateChanged

    private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
        inputsJComboBox.setModel(controller.createComboBoxModel());
        targetsJComboBox.setModel(controller.createComboBoxModel());
    }//GEN-LAST:event_formComponentShown

    private void trainJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_trainJButtonActionPerformed
        setCursor(waitCursor); 
        try {                       
            resetResults();
            graphJPanel.setBackground(new Color(0,0,0,64));
            loadTrainingInput();
            double error = getErrorToleration();
            controller.buildNetwork(layersJTextField.getText(), activationsJTextField.getText());
            controller.train(input, error, inputsJComboBox.getSelectedItem().toString(), targetsJComboBox.getSelectedItem().toString());            
            displayTextResults();
            displayGraph();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error :( - " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
        setCursor(defaultCursor);
    }//GEN-LAST:event_trainJButtonActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField activationsJTextField;
    private javax.swing.JTextField alfaJTextField;
    private javax.swing.JPanel controlsJPanel;
    private javax.swing.JTextField errorJTextField;
    private javax.swing.JPanel graphJPanel;
    private javax.swing.JComboBox inputsJComboBox;
    private javax.swing.JLabel iterationCountJLabel;
    private javax.swing.JSlider iterationsCountJSlider;
    private javax.swing.JLabel iterationsJLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lastErrorJLabel;
    private javax.swing.JTextField layersJTextField;
    private javax.swing.JLabel minErrorJLabel;
    private javax.swing.JTextField momentumJTextField;
    private javax.swing.JComboBox targetsJComboBox;
    private javax.swing.JButton trainJButton;
    // End of variables declaration//GEN-END:variables

    private void loadTrainingInput() {
        input.iterationsCount = iterationsCountJSlider.getValue();
        input.learningRate = Double.parseDouble(alfaJTextField.getText());
        input.momentum = Double.parseDouble(momentumJTextField.getText());
    }

    private double getErrorToleration() {
        return Double.parseDouble(errorJTextField.getText());
    }

    private void displayTextResults() {
        TrainingOutput output = controller.getTrainingOutput();
        minErrorJLabel.setText(String.format("%.6f", output.minError));
        lastErrorJLabel.setText(String.format("%.6f", output.lastError));
        iterationsJLabel.setText("" + output.lastIterationNumber);
    }

    private void displayGraph() {
        XYSeriesCollection collection = controller.getGraphModel();  
        Plot plot = graphs.getXYPlot(collection);        
        graphJPanel.add(graphs.getChartPanel(plot), BorderLayout.CENTER);
    }

    private void resetResults() {
        minErrorJLabel.setText("?");
        lastErrorJLabel.setText("?");
        iterationsJLabel.setText("?");
        graphJPanel.removeAll();
        graphJPanel.repaint();
    }
}
