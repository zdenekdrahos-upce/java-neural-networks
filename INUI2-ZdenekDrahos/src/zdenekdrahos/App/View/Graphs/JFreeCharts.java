
package zdenekdrahos.App.View.Graphs;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class JFreeCharts implements IJFreeCharts {
    
    @Override
    public Plot getCombinedXYPlot(XYSeries[] series, String groupPostfix) {
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot();
        plot.setGap(20.0);
        plot.setOrientation(PlotOrientation.VERTICAL);

        XYItemRenderer renderer;
        NumberAxis rangeAxisY;
        XYPlot xyPlot;

        for (int i = 0; i < series.length; i++) {
            renderer = new StandardXYItemRenderer();
            rangeAxisY = getNumberAxis();
            rangeAxisY.setLabel((i + 1) + groupPostfix);
            xyPlot = new XYPlot(new XYSeriesCollection(series[i]), null, rangeAxisY, renderer);
            xyPlot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_LEFT);
            plot.add(xyPlot);
        }
        return plot;
    }
    
    @Override
    public Plot getXYPlot(XYSeriesCollection dataset) {
        return new XYPlot(dataset, getNumberAxis(), getNumberAxis(), new StandardXYItemRenderer());  
    }

    @Override
    public ChartPanel getChartPanel(Plot plot) {
        JFreeChart chart = new JFreeChart(plot);                
        return new ChartPanel(chart);  
    }
    
    @Override
    public NumberAxis getNumberAxis() {        
        NumberAxis axis = new NumberAxis();
        axis.setAutoRangeIncludesZero(false);
        return axis;
    }
    
}
