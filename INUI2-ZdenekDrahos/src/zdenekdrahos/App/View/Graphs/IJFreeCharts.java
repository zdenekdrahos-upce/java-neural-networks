/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zdenekdrahos.App.View.Graphs;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.Plot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public interface IJFreeCharts {

    ChartPanel getChartPanel(Plot plot);

    Plot getCombinedXYPlot(XYSeries[] series, String groupPostfix);

    NumberAxis getNumberAxis();

    Plot getXYPlot(XYSeriesCollection dataset);
    
}
