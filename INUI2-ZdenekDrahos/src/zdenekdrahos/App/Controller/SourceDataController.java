
package zdenekdrahos.App.Controller;

import java.io.FileNotFoundException;
import java.util.Observable;
import javax.swing.table.DefaultTableModel;
import zdenekdrahos.Arrays.Converter2D.IStringToDouble;
import zdenekdrahos.Arrays.Converter2D.StringToDouble;
import zdenekdrahos.CsvImport.CsvImport;
import zdenekdrahos.CsvImport.ICsvImport;

public class SourceDataController extends Observable implements ISourceDataController {

    private Double[][] data;

    @Override
    public void loadCsvFile(String pathToFile, int columnsCount, char columnSeparator)
            throws FileNotFoundException {
        ICsvImport csvImport = new CsvImport();
        IStringToDouble builder = new StringToDouble();
        csvImport.importFile(pathToFile, columnsCount, columnSeparator);
        data = builder.parseStringArray(csvImport.iterator());
        onDataLoad();
    }

    @Override
    public DefaultTableModel getTableModel() {
        return new DefaultTableModel(data, getColumns());
    }

    @Override
    public Double[][] getData() {
        return data;
    }

    @Override
    public int getColumnsCount() {
        return data[0].length;
    }

    private String[] getColumns() {
        String[] columns = new String[getColumnsCount()];
        for (Integer i = 1; i <= columns.length; i++) {
            columns[i-1] = i.toString();
        }
        return columns;
    }

    private void onDataLoad() {
        setChanged();
        notifyObservers(data);
    }

}