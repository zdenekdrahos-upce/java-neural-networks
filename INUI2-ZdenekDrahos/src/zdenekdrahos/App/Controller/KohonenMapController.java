
package zdenekdrahos.App.Controller;

import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import org.jfree.data.xy.XYSeries;
import zdenekdrahos.AI.KohonenMap.IKohonenMap;

public class KohonenMapController extends Observable implements IKohonenMapController, Observer {

    private List<List<Double[]>> groups;
    private IKohonenMap kohonenMap;
    
    public KohonenMapController(IKohonenMap map) {
        kohonenMap = map;
    }
    
    @Override
    public void update(Observable o, Object arg) {
        setData((Double[][]) arg);
    }

    @Override
    public void setData(Double[][] data) {
        kohonenMap.setData(data);
    }

    @Override
    public void train(double alfa, int groupsCount, int iterationsCount) {
        kohonenMap.train(alfa, groupsCount, iterationsCount);
        kohonenMap.classifyData();
        kohonenMap.sortClassifiedData();
        groups = kohonenMap.getClassifiedData();
        onDataLoad();
    }

    @Override
    public DefaultTableModel getTableModel(String groupPostfix) {
        DefaultTableModel model = new DefaultTableModel();
        List<Double[]> currentGroup;
        String columnName, row;
        Vector columnData = new Vector();

        for (int groupIndex = 0; groupIndex < groups.size(); groupIndex++) {
            columnData.clear();
            columnName = new Integer(groupIndex + 1).toString() + groupPostfix;
            currentGroup = groups.get(groupIndex);
            for (int i = 0; i < currentGroup.size(); i++) {
                row = "";
                for (int j = 0; j < currentGroup.get(0).length; j++) {
                    row += currentGroup.get(i)[j] + "  ";
                }
                columnData.add(row);
            }
            model.addColumn(columnName, columnData);
        }
        return model;
    }

    @Override
    public XYSeries[] getGraphSeries(String groupPostfix) {
        String columnName;
        XYSeries[] series = new XYSeries[kohonenMap.getGroupsCount()];
        if (isGraph2D()) {
            for (int group = 0; group < groups.size(); group++) {
                columnName = new Integer(group + 1).toString() + groupPostfix;
                series[group] = new XYSeries(columnName);
                for (int i = 0; i < groups.get(group).size(); i++) {
                    series[group].add(groups.get(group).get(i)[0], groups.get(group).get(i)[1]);
                }
            }
        }
        return series;
    }

    @Override
    public boolean isGraph2D() {
        return groups.get(0).get(0).length == 2;
    }

    private void onDataLoad() {
        setChanged();
        notifyObservers(groups);
    }
}