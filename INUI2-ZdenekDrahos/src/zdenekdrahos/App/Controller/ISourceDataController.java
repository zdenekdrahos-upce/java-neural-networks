
package zdenekdrahos.App.Controller;

import java.io.FileNotFoundException;
import javax.swing.table.DefaultTableModel;

public interface ISourceDataController {

    public void loadCsvFile(String pathToFile, int columnsCount, char columnSeparator)
            throws FileNotFoundException;

    public DefaultTableModel getTableModel();

    public Double[][] getData();

    public int getColumnsCount();
    
}
