
package zdenekdrahos.App.Controller;

import java.util.Observable;
import javax.swing.ComboBoxModel;
import org.jfree.data.xy.XYSeriesCollection;
import zdenekdrahos.AI.Training.Output.TrainingOutput;
import zdenekdrahos.AI.Training.TrainingInput;

public interface IBackPropagationController {

    void update(Observable o, Object arg);
 
    void buildNetwork(String topology, String activationFunctions);
    
    void train(TrainingInput input, double errorToleration, String inputs, String targets);
    
    ComboBoxModel createComboBoxModel();
    
    TrainingOutput getTrainingOutput();
    
    XYSeriesCollection getGraphModel();
    
}
