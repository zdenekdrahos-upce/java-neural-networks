
package zdenekdrahos.App.Controller;

import java.util.Observable;
import javax.swing.table.DefaultTableModel;
import org.jfree.data.xy.XYSeries;

public interface IKohonenMapController {

    public void update(Observable o, Object arg);
    
    public void setData(Double[][] data);
    
    public void train(double alfa, int groupsCount, int iterationsCount);

    public DefaultTableModel getTableModel(String groupPostfix);

    public XYSeries[] getGraphSeries(String groupPostfix);

    public boolean isGraph2D();
    
}
