
package zdenekdrahos.Testing;

import zdenekdrahos.AI.FeedForward.FeedForward;
import zdenekdrahos.AI.FeedForward.IFeedForward;
import zdenekdrahos.AI.FeedForward.INetworkValues;
import zdenekdrahos.AI.NeuralNetwork.Builder.Activations;
import zdenekdrahos.AI.NeuralNetwork.Builder.INetworkBuilder;
import zdenekdrahos.AI.NeuralNetwork.Builder.NetworkBuilder;
import zdenekdrahos.AI.NeuralNetwork.INeuralNetwork;
import zdenekdrahos.AI.Training.Builder.ITrainingBuilder;
import zdenekdrahos.AI.Training.Builder.TrainingBuilder;
import zdenekdrahos.AI.Training.ITraining;
import zdenekdrahos.AI.Training.TrainingInput;
import zdenekdrahos.AI.Training.Output.TrainingOutput;

public class MainTrainingXOR {
    
    static double[][] input = {
        {0,0}, {0,1}, {1,0}, {1,1},
    };
    static double[][] output = {
        {0}, {1}, {1}, {0},
    };
    
    public static void main(String[] args) {
        INetworkBuilder builder = new NetworkBuilder();
        
        int[] topology = {2, 3, 1};
        Activations[] activations = {Activations.LIN, Activations.TANH, Activations.LIN};        
        INeuralNetwork network = builder.build(topology, activations);
        Printing.printNetworkWeights(network);
        
        TrainingInput in = new TrainingInput();
        in.iterationsCount = 3000;
        in.learningRate = 0.8;
        in.momentum = 0.798;
        in.inputs = input;
        in.targets = output;
        
        ITrainingBuilder trainBuilder = new TrainingBuilder();
        ITraining train = trainBuilder.build();
        TrainingOutput out = new TrainingOutput(0.01);
        train.train(network, in, out);
        System.out.printf("Solution in %d iteration, minError = %f, lastError: %f\n\n", out.lastIterationNumber, out.minError, out.lastError);        
        //Printing.printNetworkWeights(network);
        
        System.out.println();
        System.out.println("Prediction");
        
        IFeedForward feedForward = new FeedForward();
        INetworkValues values;
        double excepted, real;
        for (int i = 0; i < input.length; i++) {
            values = feedForward.buildNetwork(network, input[i]);
            for (int j = 0; j < output[i].length; j++) {
                excepted = output[i][j];
                real = values.getNeuronValue(network.getOutputLayerIndex(), j);
                System.out.printf("Expected %f , but result %f\n", excepted, real);
            }            
        }
        
    }

}
