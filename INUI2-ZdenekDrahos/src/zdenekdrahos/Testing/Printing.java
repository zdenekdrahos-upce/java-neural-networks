
package zdenekdrahos.Testing;

import java.util.List;
import zdenekdrahos.AI.FeedForward.INetworkValues;
import zdenekdrahos.AI.NeuralNetwork.INeuralNetwork;
import zdenekdrahos.AI.NeuralNetwork.Layers.ILayer;
import zdenekdrahos.AI.NeuralNetwork.Weights.IWeight;
import zdenekdrahos.AI.NeuralNetwork.Weights.IWeights;

public class Printing {

    public static void print(String name, Double[][] matrix) {
        System.out.println(name);
        for (int i = 0; i < matrix.length; i++) {
            System.out.print((i + 1) + ": ");
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + "   ");
            }
            System.out.println();
        }
    }

    public static void printNetworkWeights(INeuralNetwork network) {
        ILayer layer;
        List<IWeight> weights;
        IWeights networkWeights = network.getWeights();

        for (int layerIndex = 1; layerIndex < network.getLayersCount(); layerIndex++) {
            layer = network.getLayer(layerIndex);
            System.out.printf("%d -> %d layer\n", layerIndex - 1, layerIndex);
            for (int neuronIndex = 0; neuronIndex < layer.getNeuronsCount(); neuronIndex++) {
                System.out.printf("%d.neuron = ", neuronIndex);
                weights = networkWeights.getNeuronWeights(layerIndex, neuronIndex);
                for (IWeight weight : weights) {
                    System.out.printf("%f  ", weight.getWeight());
                }
                System.out.println();
            }
        }
        System.out.println();
    }

    public static void printNetworkValues(INeuralNetwork network, INetworkValues values) {
        List<Double> temp;
        for (int i = 1; i < network.getLayersCount(); i++) {
            System.out.printf("%d .layer: ", i);
            temp = values.getLayerValues(i);
            for (int j = 0; j < temp.size(); j++) {
                System.out.printf("%f  ", temp.get(j));
            }
            System.out.println();
        }
        System.out.println();
    }

}
