/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.Arrays.Converter2D;

import java.util.Iterator;

public interface IStringToDouble {
    
    public Double[][] parseStringArray(Iterator<String[]> stringIterator);
    
}