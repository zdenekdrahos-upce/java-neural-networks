/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.Arrays.Converter2D;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class StringToDouble implements IStringToDouble {

    private List<Double[]> list;
    private List<Double> currentLine;

    public StringToDouble() {
        list = new ArrayList<Double[]>();
        currentLine = new ArrayList<Double>();
    }

    @Override
    public Double[][] parseStringArray(Iterator<String[]> stringIterator) {
        prepareBeforeParse();
        for (Iterator<String[]> it = stringIterator; it.hasNext();) {
            buildLine(it.next());
            addLineToList();
        }
        return convertListToArray();
    }

    private void prepareBeforeParse() {
        list.clear();
    }

    private void buildLine(String[] line) {
        currentLine.clear();
        for (int i = 0; i < line.length; i++) {
            currentLine.add(Double.parseDouble(line[i]));
        }
    }

    private void addLineToList() {
        list.add(currentLine.toArray(new Double[0]));
    }

    private Double[][] convertListToArray() {        
        return list.toArray(new Double[0][0]);
    }
    
}
