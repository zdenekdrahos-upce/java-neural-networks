/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.Statistics.MinSearch;

public interface IMinSearch {

    void findClosestWeight(Double[][] weights, Double[] data);
    
    void findExtrems(double[] data);

    int getIndexOfMin();
    
    double getValueOfMin();

}
