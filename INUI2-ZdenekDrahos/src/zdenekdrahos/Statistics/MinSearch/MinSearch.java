/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.Statistics.MinSearch;

import zdenekdrahos.Statistics.Errors.ILeastSquares;
import zdenekdrahos.Statistics.Errors.LeastSquares;

public class MinSearch implements IMinSearch {

    private int indexMinimum;
    private double valueMinimum;
    private ILeastSquares leastSquares = new LeastSquares();
    
    @Override
    public void findClosestWeight(Double[][] weights, Double[] data) {
        int groupsCount = weights.length;
        double[] differences = new double[groupsCount];
        for (int groupIndex = 0; groupIndex < groupsCount; groupIndex++) {            
            differences[groupIndex] = leastSquares.getError(weights[groupIndex], data);
        }
        findExtrems(differences);
    }

    @Override
    public void findExtrems(double[] data) {
        valueMinimum = Double.MAX_VALUE;
        indexMinimum = -1;
        for (int i = 0; i < data.length; i++) {
            if (data[i] <= valueMinimum) {
                valueMinimum = data[i];
                indexMinimum = i;
            }
        }
    }

    @Override
    public int getIndexOfMin() {
        return indexMinimum;
    }

    @Override
    public double getValueOfMin() {
        return valueMinimum;
    }

}
