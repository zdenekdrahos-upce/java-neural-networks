/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.Statistics.Characteristics;

public class Mean implements IMean{

    @Override
    public double getMean(Double sum, int count) {
        return sum / (double) count;
    }

}
