/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.Statistics.Characteristics;

public interface IMean {

    public double getMean(Double sum, int count);
    
}
