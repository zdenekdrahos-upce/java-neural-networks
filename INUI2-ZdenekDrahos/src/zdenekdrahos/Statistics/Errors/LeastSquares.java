/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.Statistics.Errors;

public class LeastSquares implements ILeastSquares {

    @Override
    public double getError(Double[] expected, Double[] real) {
        double error = 0;
        for (int i = 0; i < expected.length; i++) {
            error += getError(expected[i], real[i]);
        }
        return error;
    }

    @Override
    public double getError(Double expected, Double real) {
        return Math.pow(expected - real, 2);
    }
}
