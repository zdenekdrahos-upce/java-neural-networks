/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.Statistics.Errors;

public interface ILeastSquares extends IErrorCalculator {

    double getError(Double[] input, Double[] output);
    
}
