/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.BackPropagation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import zdenekdrahos.AI.NeuralNetwork.INeuralNetwork;

public class PreviousAdjustments {

    private Map<Integer, List<List<Double>>> previousAdjustment;

    public PreviousAdjustments() {
        previousAdjustment = new HashMap<Integer, List<List<Double>>>();
    }

    public void initPreviousAdjustment(INeuralNetwork network) {
        previousAdjustment.clear();
        for (int layerIndex = 1; layerIndex < network.getLayersCount(); layerIndex++) {
            int previousNeurons = network.getLayer(layerIndex - 1).getNeuronsCount() + 1;
            int currentNeurons = network.getLayer(layerIndex).getNeuronsCount();
            List<List<Double>> initMomentum = new ArrayList<List<Double>>(currentNeurons);
            for (int neuronIndex = 0; neuronIndex < currentNeurons; neuronIndex++) {
                List<Double> momentumValues = new ArrayList<Double>(previousNeurons);
                for (int prevIndex = 0; prevIndex < previousNeurons; prevIndex++) {
                    momentumValues.add(0.0);
                }
                initMomentum.add(momentumValues);
            }
            previousAdjustment.put(layerIndex, initMomentum);
        }
    }

    public double get(int layerIndex, int neuronIndex, int previousNeuronIndex) {
        return previousAdjustment.get(layerIndex).get(neuronIndex).get(previousNeuronIndex);
    }

    public void set(int layerIndex, int neuronIndex, int previousNeuronIndex, double weightAdjustment) {
        previousAdjustment.get(layerIndex).get(neuronIndex).set(previousNeuronIndex, weightAdjustment); 
    }
    
}
