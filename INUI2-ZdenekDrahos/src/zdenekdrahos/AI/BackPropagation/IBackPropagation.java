/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.BackPropagation;

import zdenekdrahos.AI.FeedForward.INetworkValues;
import zdenekdrahos.AI.NeuralNetwork.INeuralNetwork;

public interface IBackPropagation {

    void setLearningRate(double learningRate);

    void setMomentum(double momentum);

    void trainNetwork(INeuralNetwork network, INetworkValues networkValues, double[] target);
    
}
