/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.BackPropagation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import zdenekdrahos.AI.NeuralNetwork.INeuralNetwork;

public class WeightErrors {
    
    private Map<Integer, List<Double>> delta;
    private List<Double> currentDelta;
    
    public WeightErrors() {
        delta = new HashMap<Integer, List<Double>>();
    }

    public void initDelta(INeuralNetwork network) {
        delta.clear();
        List<Double> deltaValues;
        for (int layerIndex = 0; layerIndex < network.getLayersCount(); layerIndex++) {
            int neurons = network.getLayer(layerIndex).getNeuronsCount();
            deltaValues = new ArrayList<Double>(neurons);
            for (int neuronIndex = 0; neuronIndex < neurons; neuronIndex++) {                
                deltaValues.add(0.0);
            }
            delta.put(layerIndex, deltaValues);
        }
    }

    public double get(int layerIndex, int neuronIndex) {
        return delta.get(layerIndex).get(neuronIndex);
    }

    public void setEditedLayer(int layerIndex) {
        currentDelta = delta.get(layerIndex);
    }

    public void setNeuronError(int neuronIndex, double error) {
        currentDelta.set(neuronIndex, error);
    }

}
