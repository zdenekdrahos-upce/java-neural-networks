/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.FeedForward;

import java.util.List;
import zdenekdrahos.AI.NeuralNetwork.INeuralNetwork;

public interface INetworkValues {

    void init(INeuralNetwork network, double[] input);

    List<Double> getLayerValues(int layerIndex);

    Double getNeuronValue(int layerIndex, int neuronIndex);

    void addNeuronValue(int layerIndex, int neuronIndex, double value);
    
}
