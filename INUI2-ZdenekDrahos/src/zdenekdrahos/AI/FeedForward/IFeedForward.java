/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.FeedForward;

import zdenekdrahos.AI.NeuralNetwork.INeuralNetwork;

public interface IFeedForward {

    INetworkValues buildNetwork(INeuralNetwork network, double[] input);
    
}
