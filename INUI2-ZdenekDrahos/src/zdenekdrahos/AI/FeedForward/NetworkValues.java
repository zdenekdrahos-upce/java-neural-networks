/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.FeedForward;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import zdenekdrahos.AI.NeuralNetwork.INeuralNetwork;

public class NetworkValues implements INetworkValues {

    private Map<Integer, List<Double>> layerValues;

    public NetworkValues() {
        layerValues = new HashMap<Integer, List<Double>>();
    }

    @Override
    public void init(INeuralNetwork network, double[] input) {
        ArrayList<Double> neurons;
        for (int layerIndex = 0; layerIndex < network.getLayersCount(); layerIndex++) {
            int neuronsCount = network.getLayer(layerIndex).getNeuronsCount();
            neurons = new ArrayList<Double>(neuronsCount);            
            for (int neuronIndex = 0; neuronIndex < neuronsCount; neuronIndex++) {
                double w = layerIndex == 0 ? input[neuronIndex] : Double.NaN;
                neurons.add(w);
            }
            layerValues.put(layerIndex, neurons);
        }
    }

    @Override
    public List<Double> getLayerValues(int layerIndex) {
        return layerValues.get(layerIndex);
    }

    @Override
    public Double getNeuronValue(int layerIndex, int neuronIndex) {
        return layerValues.get(layerIndex).get(neuronIndex);
    }

    @Override
    public void addNeuronValue(int layerIndex, int neuronIndex, double value) {
        layerValues.get(layerIndex).set(neuronIndex, value);
    }
}
