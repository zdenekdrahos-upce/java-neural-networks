/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.FeedForward;

import java.util.List;
import zdenekdrahos.AI.ActivationFunctions.IActivationFunction;
import zdenekdrahos.AI.NeuralNetwork.INeuralNetwork;
import zdenekdrahos.AI.NeuralNetwork.Layers.ILayer;
import zdenekdrahos.AI.NeuralNetwork.Weights.IWeight;

public class FeedForward implements IFeedForward {

    private INetworkValues values;
    private ILayer currentLayer;
    private List<Double> previousLayerValues;
    private List<IWeight> previousLayerWeights;

    @Override
    public INetworkValues buildNetwork(INeuralNetwork network, double[] input) {
        values = new NetworkValues();
        values.init(network, input);
        feedforward(network);
        return values;
    }

    private void feedforward(INeuralNetwork network) {
        for (int layerIndex = 1; layerIndex < network.getLayersCount(); layerIndex++) {
            currentLayer = network.getLayer(layerIndex);
            previousLayerValues = values.getLayerValues(layerIndex - 1);
            for (int neuronIndex = 0; neuronIndex < currentLayer.getNeuronsCount(); neuronIndex++) {
                previousLayerWeights = network.getWeights().getNeuronWeights(layerIndex, neuronIndex);
                values.addNeuronValue(layerIndex, neuronIndex, getNeuronOutput());
            }
        }
    }

    private double getNeuronOutput() {
        double inputSum = getInputSum();
        IActivationFunction activationFunction = currentLayer.getActivationFunction();
        return activationFunction.activate(inputSum);
    }

    private double getInputSum() {
        double sum = 0;
        sum += getBias();
        for (int previousNeuronIndex = 1; previousNeuronIndex < previousLayerWeights.size(); previousNeuronIndex++) {
            sum += getNeuronSignal(previousNeuronIndex);
        }
        return sum;
    }

    private double getBias() {
        return 1 * previousLayerWeights.get(0).getWeight();
    }

    private double getNeuronSignal(int previousNeuronIndex) {
        double neuronValue = previousLayerValues.get(previousNeuronIndex - 1);
        double weight = previousLayerWeights.get(previousNeuronIndex).getWeight();
        return neuronValue * weight;
    }
}
