/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.ActivationFunctions;

public class Sigmoid implements IActivationFunction {

    @Override
    public double activate(double x) {
        if (x < 15 && x > -15 && x != Double.MIN_VALUE && x != Double.MAX_VALUE) {
            return 1.0 / (1.0 + Math.exp(-1 * x));
        } else if (x < -15 || x == Double.MIN_VALUE) {
            return 0;
        } else {
            return 1;
        }
    }
    
    @Override
    public double derivate(double x) {
        return x * (1 - x);
    }
}
