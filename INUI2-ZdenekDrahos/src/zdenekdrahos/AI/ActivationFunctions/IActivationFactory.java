/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.ActivationFunctions;

public interface IActivationFactory {

    IActivationFunction getHyperbolicTangent();

    IActivationFunction getLinearFunction();

    IActivationFunction getSigmoid();
    
}
