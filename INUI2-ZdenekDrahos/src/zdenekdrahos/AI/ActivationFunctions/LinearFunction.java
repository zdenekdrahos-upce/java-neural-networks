/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.ActivationFunctions;

public class LinearFunction implements IActivationFunction {

    private double start, sigma;
    
    public LinearFunction() {
        this(1);
    }

    public LinearFunction(double sigma) {
        start = 0;
        this.sigma = sigma;
    }
    
    @Override
    public double activate(double x) {
        return start + sigma * x;
    }
    
    @Override
    public double derivate(double x) {
        return sigma;
    }
    
}
