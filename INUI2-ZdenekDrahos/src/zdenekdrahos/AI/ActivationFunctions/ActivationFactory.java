/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.ActivationFunctions;

public class ActivationFactory implements IActivationFactory {

    private double sigma;

    public ActivationFactory() {
        this(1);
    }

    public ActivationFactory(double sigma) {
        this.sigma = sigma;
    }

    @Override
    public IActivationFunction getLinearFunction() {
        return new LinearFunction(sigma);
    }

    @Override
    public IActivationFunction getHyperbolicTangent() {
        return new HyperbolicTangent();
    }

    @Override
    public IActivationFunction getSigmoid() {
        return new Sigmoid();
    }

}
