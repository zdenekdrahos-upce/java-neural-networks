/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.ActivationFunctions;

public class HyperbolicTangent implements IActivationFunction {

    @Override
    public double activate(double x) {
        if (x == Double.MIN_VALUE)
            return -1;
        else if (x == Double.MAX_VALUE)
            return 1;
        else 
            return Math.tanh(x);
    }
    
    @Override
    public double derivate(double x) {
        return 1 - x * x;
    }

}
