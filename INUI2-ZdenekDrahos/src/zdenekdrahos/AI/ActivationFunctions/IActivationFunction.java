/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.ActivationFunctions;

public interface IActivationFunction {

    public double activate(double x);
    
    public double derivate(double x);
    
}
