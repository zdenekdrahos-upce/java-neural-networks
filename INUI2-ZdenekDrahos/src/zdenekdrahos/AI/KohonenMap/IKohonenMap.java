/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.KohonenMap;

import java.util.List;

public interface IKohonenMap {

    void setData(Double[][] data);

    void train(double alfa, int groupsCount, int iterationsCount);

    int getGroupsCount();
    
    Double[][] getWeights();

    void classifyData();

    void sortClassifiedData();

    List<List<Double[]>> getClassifiedData();
    
}
