/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.KohonenMap;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class KohonenMap implements IKohonenMap {

    private Double[][] data;
    private List<List<Double[]>> groups;
    private KohonenTraining training = new KohonenTraining();
    private KohonenClassification classification = new KohonenClassification();

    @Override
    public void setData(Double[][] data) {
        this.data = data;
    }

    @Override
    public void train(double alfa, int groupsCount, int iterationsCount) {
        if (data != null) {
            training.train(data, alfa, groupsCount, iterationsCount);            
        } else {
            throw new NullPointerException("Data not set");
        }
    }
    
    @Override
    public int getGroupsCount() {
        return training.getGroupsCount();
    }

    @Override
    public Double[][] getWeights() {
        return training.getWeights();
    }

    @Override
    public void classifyData() {
        groups = classification.classify(training.getWeights(), data);
    }

    @Override
    public void sortClassifiedData() {
        for (int group = 0; group < groups.size(); group++) {
            Collections.sort(groups.get(group), comparer());
        }     
    }

    @Override
    public List<List<Double[]>> getClassifiedData() {
        return groups;
    }

    
    private Comparator<Double[]> comparer() {
        return new Comparator<Double[]>() {

            @Override
            public int compare(Double[] a, Double[] b) {
                return a[0].compareTo(b[0]);
            }

        };
    }
    
    
}
