/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.KohonenMap;

import java.util.Random;
import zdenekdrahos.Statistics.MinSearch.IMinSearch;
import zdenekdrahos.Statistics.MinSearch.MinSearch;

public class KohonenTraining {

    private Double[][] input;
    private int inputCount, neuronsCount, groupsCount, neighbourhood;
    private double learningRate;
    private Double[][] weights;
    private IMinSearch minSearch = new MinSearch();

    public Double[][] getWeights() {
        return weights;
    }

    public int getGroupsCount() {
        return groupsCount;
    }

    public void train(Double[][] inputData, double alfa, int groups, int iterationsCount) {
        input = inputData;
        groupsCount = groups;
        learningRate = alfa;
        neighbourhood = getOriginNeighbourhood();
        inputCount = inputData.length;
        neuronsCount = inputData[0].length;
        weights = getRandMatrix();
        train(iterationsCount);
    }

    private void train(int iterationsCount) {
        for (int k = 1; k <= iterationsCount; k++) {
            for (int j = 0; j < inputCount; j++) {
                iteration(input[j]);
            }
            updateLearningRate();
            updateNeighbourhood(iterationsCount, k);
        }
    }

    private void iteration(Double[] pattern) {
        minSearch.findClosestWeight(weights, pattern);
        int minIndex = Math.max(minSearch.getIndexOfMin() - neighbourhood, 0);
        int maxIndex = Math.min(minSearch.getIndexOfMin() + neighbourhood, groupsCount - 1);
        for (int groupIndex = minIndex; groupIndex <= maxIndex; groupIndex++) {
            for (int k = 0; k < pattern.length; k++) {
                weights[groupIndex][k] = weights[groupIndex][k] + learningRate * (pattern[k] - weights[groupIndex][k]);
            }
        }
    }

    private void updateLearningRate() {
        learningRate = learningRate * 0.99;
    }

    private void updateNeighbourhood(int iterationsCount, int actualIteration) {
        if (neighbourhood > 0) {
            neighbourhood = Math.round((iterationsCount - actualIteration) / (float) (iterationsCount * neighbourhood));
        }
    }

    private int getOriginNeighbourhood() {
        return (int) Math.floor(groupsCount / 2.0);
    }

    private Double[][] getRandMatrix() {
        Random generator = new Random();
        Double[][] matrix = new Double[groupsCount][neuronsCount];
        for (int i = 0; i < groupsCount; i++) {
            for (int j = 0; j < neuronsCount; j++) {
                matrix[i][j] = generator.nextGaussian();
            }
        }
        return matrix;
    }
}
