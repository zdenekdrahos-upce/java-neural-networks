/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.KohonenMap;

import java.util.ArrayList;
import java.util.List;
import zdenekdrahos.Statistics.MinSearch.IMinSearch;
import zdenekdrahos.Statistics.MinSearch.MinSearch;

public class KohonenClassification {

    private List<List<Double[]>> lists;
    private IMinSearch minSearch = new MinSearch();
    
    public List<List<Double[]>> classify(Double[][] weights, Double[][] data) {        
        initLists(weights);
        classifyData(weights, data);
        return lists;
    }

    private void initLists(Double[][] weights) {
        lists = new ArrayList<List<Double[]>>();        
        for (int group = 0; group < weights.length; group++) {
            lists.add(0, new ArrayList<Double[]>());
        }
    }

    private void classifyData(Double[][] weights, Double[][] data) {
        int groupIndex;
        for (int i = 0; i < data.length; i++) {
            groupIndex = classify(weights, data[i]);
            lists.get(groupIndex).add(data[i]);
        }
    }
    
    private int classify(Double[][] weights, Double[] data) {
        minSearch.findClosestWeight(weights, data);
        return minSearch.getIndexOfMin();
    }

}
