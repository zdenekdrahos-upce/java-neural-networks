/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.Training.Simulation;

import java.util.List;
import zdenekdrahos.AI.FeedForward.FeedForward;
import zdenekdrahos.AI.FeedForward.IFeedForward;
import zdenekdrahos.AI.FeedForward.INetworkValues;
import zdenekdrahos.AI.NeuralNetwork.INeuralNetwork;

public class Simulation implements ISimulation {

    private IFeedForward feedForward = new FeedForward();
    private INeuralNetwork network;
    private int indexOfOutputLayer;
    private List<Double> lastSimulation;

    @Override
    public void setNetwork(INeuralNetwork network) {
        this.network = network;
        indexOfOutputLayer = network.getOutputLayerIndex();
    }

    @Override
    public void simulate(double[] inputPattern) {
        if (network == null) {
            throw new NullPointerException("Network");
        }
        INetworkValues values = feedForward.buildNetwork(network, inputPattern);
        lastSimulation = values.getLayerValues(indexOfOutputLayer);
    }

    @Override
    public Double get(int neuronIndex) {
        if (network == null) {
            throw new NullPointerException("Network");
        }
        return lastSimulation.get(neuronIndex);
    }
}
