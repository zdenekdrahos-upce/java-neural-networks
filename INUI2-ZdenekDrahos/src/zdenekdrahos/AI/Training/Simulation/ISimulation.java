/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.Training.Simulation;

import zdenekdrahos.AI.NeuralNetwork.INeuralNetwork;

public interface ISimulation {

    void setNetwork(INeuralNetwork network);

    void simulate(double[] inputPattern);
    
    Double get(int neuronIndex);

}
