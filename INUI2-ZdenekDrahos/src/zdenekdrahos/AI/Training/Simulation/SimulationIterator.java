/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.Training.Simulation;

import java.util.Iterator;
import zdenekdrahos.AI.NeuralNetwork.INeuralNetwork;

public class SimulationIterator implements Iterator<Double> {

    private ISimulation simulation;
    private int actualIndex, patternLength;
    
    public SimulationIterator(ISimulation simulation) {
        this.simulation = simulation;
    }

    public void simulate(INeuralNetwork network, double[] inputPattern) {
        simulation.setNetwork(network);
        simulation.simulate(inputPattern);
        actualIndex = 0;
        patternLength = inputPattern.length;
    }

    @Override
    public boolean hasNext() {
        return actualIndex < patternLength;
    }

    @Override
    public Double next() {
        return simulation.get(actualIndex++);
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    
}
