/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.Training;

public class TrainingInput {

    public int iterationsCount;
    
    public double learningRate;
    
    public double momentum;
    
    public double[][] inputs;
    
    public double[][] targets;

}
