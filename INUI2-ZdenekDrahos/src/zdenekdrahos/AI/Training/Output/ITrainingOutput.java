/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.Training.Output;

public interface ITrainingOutput {

    void processError(double meanError);

    boolean continueTraining();

}
