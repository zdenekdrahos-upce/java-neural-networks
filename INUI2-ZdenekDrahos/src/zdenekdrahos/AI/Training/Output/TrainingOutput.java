/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.Training.Output;

public class TrainingOutput implements ITrainingOutput {
    
    public int lastIterationNumber;    
    public double minError, lastError, errorTolerance;

    public TrainingOutput(double errorTolerance) {
        lastIterationNumber = 0;
        minError = Double.MAX_VALUE;
        lastError = Double.MAX_VALUE;
        this.errorTolerance = errorTolerance;
    }

    @Override
    public void processError(double meanError) {
        lastIterationNumber++;
        lastError = meanError;
        if (meanError < minError) {
            minError = meanError;
        }
    }

    @Override
    public boolean continueTraining() {
        return minError > errorTolerance;
    }

}
