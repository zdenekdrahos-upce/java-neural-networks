/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.Training.Sets;

import java.util.ArrayList;
import java.util.List;

public class DataSet {

    public List<double[]> inputs;
    public List<double[]> targets;

    public DataSet() {
        inputs = new ArrayList<double[]>();
        targets = new ArrayList<double[]>();
    }

    public void clear() {
        inputs.clear();
        targets.clear();
    }

    public int size() {
        return inputs.size();
    }

    public void add(double[] inputPattern, double[] outputPattern) {
        inputs.add(inputPattern);
        targets.add(outputPattern);
    }

}
