/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.Training.Sets;

public class DataSeparator implements IDataSeparator {


    @Override
    public DataSet[] separate(double[][] inputs, double[][] targets) {
        DataSet[] sets = {new DataSet(), new DataSet()};
        for (int i = 0; i < inputs.length; i++) {
            if (i % 3 == 1) {
                sets[1].add(inputs[i], targets[i]);
            } else {
                sets[0].add(inputs[i], targets[i]);
            }
        }
        return sets;
    }

}
