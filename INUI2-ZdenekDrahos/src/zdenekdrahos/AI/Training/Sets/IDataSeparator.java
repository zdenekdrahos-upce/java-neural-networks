/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.Training.Sets;

public interface IDataSeparator {

    DataSet[] separate(double[][] inputs, double[][] targets);
    
}
