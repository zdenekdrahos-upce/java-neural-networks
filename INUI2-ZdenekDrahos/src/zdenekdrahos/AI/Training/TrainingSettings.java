/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.Training;

import zdenekdrahos.AI.BackPropagation.IBackPropagation;
import zdenekdrahos.AI.FeedForward.IFeedForward;
import zdenekdrahos.AI.Training.Simulation.ISimulation;
import zdenekdrahos.AI.Training.Sets.IDataSeparator;
import zdenekdrahos.Statistics.Characteristics.IMean;
import zdenekdrahos.Statistics.Errors.IErrorCalculator;

public class TrainingSettings {

    // Neural networks    
    public IDataSeparator separator;

    public IFeedForward feedForward;

    public IBackPropagation backPropagation;

    public ISimulation simulation;

    // Error calculation - statistics etc.
    public IErrorCalculator errorCalculator;

    public IMean meanCalculator;
    
}
