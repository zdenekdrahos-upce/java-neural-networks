/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.Training.Validator;

import zdenekdrahos.AI.NeuralNetwork.INeuralNetwork;
import zdenekdrahos.AI.NeuralNetwork.Layers.ILayer;
import zdenekdrahos.AI.NeuralNetwork.Weights.IWeights;
import zdenekdrahos.AI.Training.TrainingInput;
import zdenekdrahos.AI.Training.TrainingSettings;

public class TrainingValidator implements ITrainingValidator {

    @Override
    public void checkTraining(INeuralNetwork network, TrainingInput input) {
        checkInstance(network);
        checkInstance(input);
        checkNetwork(network);
        checkInput(input);
        checkNetworkPatterns(network, input.inputs, input.targets);
    }

    @Override
    public void checkSettings(TrainingSettings settings) {
        checkInstance(settings.backPropagation);
        checkInstance(settings.errorCalculator);
        checkInstance(settings.feedForward);
        checkInstance(settings.meanCalculator);
        checkInstance(settings.simulation);
        checkInstance(settings.separator);
    }
    
    private void checkInstance(Object instance) {
        if (instance == null)
            throw new NullPointerException();
    }

    private void checkNetwork(INeuralNetwork network) {
        checkTopology(network.getLayersCount());
        checkWeights(network.getLayersCount(), network.getWeights());
    }

    private void checkTopology(int layersCount) {
        if (layersCount <= 1)
            throw new IllegalArgumentException("At least 2 layers");
    }

    private void checkWeights(int layersCount, IWeights weights) {
        checkInstance(weights);
        checkLayersInWeights(layersCount, weights.getLayersCount());
    }

    private void checkLayersInWeights(int layersCount, int weightsLayers) {
        if (layersCount != weightsLayers)
            throw new IllegalArgumentException("Initial weights are not set");
    }

    private void checkInput(TrainingInput input) {
        checkInstance(input.inputs);
        checkInstance(input.targets);
        checkPattersLength(input.inputs.length, input.targets.length);
    }

    private void checkPattersLength(int lengthInput, int lengthOutput) {        
        if (lengthInput != lengthOutput)
            throw new IllegalArgumentException("Different length of input and output");
    }

    private void checkNetworkPatterns(INeuralNetwork network, double[][] inputPatterns, double[][] outputPatterns) {        
        checkNeuronsPatternsCount(network.getInputLayer(), inputPatterns);
        checkNeuronsPatternsCount(network.getOutputLayer(), outputPatterns);
    }

    private void checkNeuronsPatternsCount(ILayer layer, double[][] patterns) {
        if (layer.getNeuronsCount() != patterns[0].length)
            throw new IllegalArgumentException("Different neurons count and pattern length");
    }

}
