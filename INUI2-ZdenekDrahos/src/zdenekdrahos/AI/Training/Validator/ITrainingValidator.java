/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.Training.Validator;

import zdenekdrahos.AI.NeuralNetwork.INeuralNetwork;
import zdenekdrahos.AI.Training.TrainingInput;
import zdenekdrahos.AI.Training.TrainingSettings;

public interface ITrainingValidator {

    void checkTraining(INeuralNetwork network, TrainingInput input);

    void checkSettings(TrainingSettings settings);

}
