/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.Training;

import zdenekdrahos.AI.Training.Output.ITrainingOutput;
import zdenekdrahos.AI.Training.Simulation.SimulationIterator;
import zdenekdrahos.AI.NeuralNetwork.INeuralNetwork;

public interface ITraining {

    void train(INeuralNetwork network, TrainingInput input, ITrainingOutput output);

    SimulationIterator simulate(double[] inputPattern);
    
}
