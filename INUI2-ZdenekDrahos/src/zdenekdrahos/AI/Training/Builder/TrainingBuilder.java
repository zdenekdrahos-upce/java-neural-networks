/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.Training.Builder;

import zdenekdrahos.AI.BackPropagation.BackPropagation;
import zdenekdrahos.AI.FeedForward.FeedForward;
import zdenekdrahos.AI.Training.Simulation.Simulation;
import zdenekdrahos.AI.Training.ITraining;
import zdenekdrahos.AI.Training.Sets.DataSeparator;
import zdenekdrahos.AI.Training.Training;
import zdenekdrahos.AI.Training.TrainingSettings;
import zdenekdrahos.Statistics.Characteristics.Mean;
import zdenekdrahos.Statistics.Errors.LeastSquares;

public class TrainingBuilder implements ITrainingBuilder {

    private TrainingSettings settings;

    public TrainingBuilder() {
        loadDefaultSettings();
    }

    @Override
    public void setSettings(TrainingSettings setttings) {
        this.settings = setttings;
    }

    @Override
    public ITraining build() {
        return new Training(settings);
    }

    private void loadDefaultSettings() {
        settings = new TrainingSettings();
        settings.separator = new DataSeparator();
        settings.feedForward = new FeedForward();
        settings.backPropagation = new BackPropagation();
        settings.simulation = new Simulation();
        settings.errorCalculator = new LeastSquares();
        settings.meanCalculator = new Mean();
    }
}
