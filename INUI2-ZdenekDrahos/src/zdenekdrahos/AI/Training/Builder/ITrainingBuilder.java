/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.Training.Builder;

import zdenekdrahos.AI.Training.ITraining;
import zdenekdrahos.AI.Training.TrainingSettings;

public interface ITrainingBuilder {

    void setSettings(TrainingSettings setttings);

    ITraining build();
    
}
