/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.NeuralNetwork.Builder;

import zdenekdrahos.AI.NeuralNetwork.INeuralNetwork;

public interface INetworkBuilder {

    void setDefaultHiddenLayer(Activations hiddenLayer);

    void setDefaultOutputLayer(Activations outputLayer);

    INeuralNetwork build(int[] topology);

    INeuralNetwork build(int[] topology, Activations[] functions);
    
}
