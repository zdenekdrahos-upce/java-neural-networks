/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.NeuralNetwork.Weights;

public interface IWeight {

    double getWeight();
    
    void setWeight(double weight);

    void generateWeight();
    
}
