/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.NeuralNetwork.Weights;

import java.util.Random;

public class Weight implements IWeight {

    private static Random generator = new Random(System.currentTimeMillis());
    
    private double weight;

    @Override
    public double getWeight() {
        return weight;
    }

    @Override
    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public void generateWeight() {
        weight = generator.nextDouble() - 0.5;
    }

}
