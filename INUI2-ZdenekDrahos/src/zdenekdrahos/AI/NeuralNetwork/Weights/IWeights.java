/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.NeuralNetwork.Weights;

import java.util.List;
import zdenekdrahos.AI.NeuralNetwork.INeuralNetwork;

public interface IWeights {

    void create(INeuralNetwork network);

    void create(INeuralNetwork network, double[][][] initialWeights);

    IWeight getConnectionWeight(int layerIndex, int neuronIndex, int previousNeuronIndex);

    List<IWeight> getNeuronWeights(int layerIndex, int neuronIndex);

    void setConnectionWeight(int layerIndex, int neuronIndex, int previousNeuronIndex, double newWeight);

    int getLayersCount();

}
