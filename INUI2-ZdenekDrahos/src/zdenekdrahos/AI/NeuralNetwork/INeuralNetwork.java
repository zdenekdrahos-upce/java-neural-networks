/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.NeuralNetwork;

import zdenekdrahos.AI.NeuralNetwork.Layers.ILayer;
import zdenekdrahos.AI.NeuralNetwork.Weights.IWeights;

public interface INeuralNetwork {

    void clear();
    
    int getLayersCount();
    
    void addLayer(ILayer layer);
    
    boolean isOutputLayer(int index);
    
    int getOutputLayerIndex();
    
    ILayer getInputLayer();
    
    ILayer getOutputLayer();
    
    ILayer getLayer(int index);
    
    IWeights getWeights();
    
    void generateWeights();
    
    void setWeights(double[][][] initialWeights);

}
