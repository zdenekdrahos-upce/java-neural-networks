/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.NeuralNetwork;

import java.util.ArrayList;
import java.util.List;
import zdenekdrahos.AI.NeuralNetwork.Layers.ILayer;
import zdenekdrahos.AI.NeuralNetwork.Layers.ILayerValidator;
import zdenekdrahos.AI.NeuralNetwork.Layers.LayerValidator;
import zdenekdrahos.AI.NeuralNetwork.Weights.IWeights;
import zdenekdrahos.AI.NeuralNetwork.Weights.Weights;

public class NeuralNetwork implements INeuralNetwork {

    private IWeights weights;
    private List<ILayer> layers;
    private ILayerValidator validator;

    public NeuralNetwork() {
        weights = new Weights();
        layers = new ArrayList<ILayer>();
        validator = new LayerValidator();
    }

    @Override
    public void clear() {
        layers.clear();
    }

    @Override
    public int getLayersCount() {
        return layers.size();
    }

    @Override
    public void addLayer(ILayer layer) {
        validator.checkLayer(layer);
        layers.add(layer);        
    }
    
    @Override
    public boolean isOutputLayer(int index) {
        return index == getOutputLayerIndex();
    }
    
    @Override
    public int getOutputLayerIndex() {
        return layers.size() - 1;
    }
    
    @Override
    public ILayer getInputLayer() {
        return getLayer(0);
    }
    
    @Override
    public ILayer getOutputLayer() {
        return getLayer(layers.size() - 1);
    }

    @Override
    public ILayer getLayer(int index) {
        return layers.get(index);
    }

    @Override
    public IWeights getWeights() {
        return weights;
    }

    @Override
    public void generateWeights() {
        weights.create(this);
    }

    @Override
    public void setWeights(double[][][] initialWeights) {
        weights.create(this, initialWeights);
    }
}
