/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.NeuralNetwork.Layers;

public class LayerValidator implements ILayerValidator {

    private ILayer layer;

    @Override
    public void checkLayer(ILayer layer) {
        this.layer = layer;
        checkInstance();
        checkNeuronsCount();
        checkActivationFunction();
    }

    private void checkInstance() {
        if (layer == null)
            throw new NullPointerException("Layer");
    }

    private void checkNeuronsCount() {
        if (layer.getNeuronsCount() <= 0)
            throw new IllegalArgumentException("Layer - neurons count");
    }

    private void checkActivationFunction() {
        if (layer.getActivationFunction() == null)
            throw new NullPointerException("Layer - activation function");
    }

}
