/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.NeuralNetwork.Layers;

import zdenekdrahos.AI.ActivationFunctions.IActivationFunction;

public interface ILayer {

    IActivationFunction getActivationFunction();

    int getNeuronsCount();
    
}
