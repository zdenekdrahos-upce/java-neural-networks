/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.AI.NeuralNetwork.Layers;

import zdenekdrahos.AI.ActivationFunctions.IActivationFunction;

public class Layer implements ILayer {

    private int neuronsCount;
    private IActivationFunction activationFunction;
    
    public Layer(int neuronsCount, IActivationFunction activationFunction) {
        this.neuronsCount = neuronsCount;
        this.activationFunction = activationFunction;
    }

    @Override
    public int getNeuronsCount() {
        return neuronsCount;
    }

    @Override
    public IActivationFunction getActivationFunction() {
        return activationFunction;
    }

}
