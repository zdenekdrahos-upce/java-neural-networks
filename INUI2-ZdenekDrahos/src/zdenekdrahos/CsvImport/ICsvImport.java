/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.CsvImport;

import java.io.FileNotFoundException;
import java.util.Iterator;

public interface ICsvImport {

    /**
     * @param pathToCsvFile
     * @param numbeOfColumns
     * @param columnSeparator
     * @return
     * @throws FileNotFoundException 
     */
    public void importFile(String pathToCsvFile, int numbeOfColumns, char columnSeparator) throws FileNotFoundException;

    public Iterator<String[]> iterator();

}
