/*
 * JAVA Neural Networks (https://bitbucket.org/zdenekdrahos/java-neural-networks)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package zdenekdrahos.CsvImport;

import au.com.bytecode.opencsv.CSVReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

public class CsvImport implements ICsvImport {

    private CSVReader csvReader;
    private int numberOfColumns;
    
    @Override
    public void importFile(String pathToCsvFile, int numbeOfColumns, char columnSeparator) throws FileNotFoundException {
        this.csvReader = new CSVReader(new FileReader(pathToCsvFile), columnSeparator);
        this.numberOfColumns = numbeOfColumns;
    }

    @Override
    public Iterator<String[]> iterator() {
        return new Iterator<String[]>() {

            private String[] nextLine;
            
            @Override
            public boolean hasNext() {
                try {   
                    nextLine = csvReader.readNext();
                    if (nextLine != null)
                        return true;
                } catch (Exception ex) { 
                }
                return false;
            }

            @Override
            public String[] next() {
                String[] result = new String[numberOfColumns];
                for (int i = 0; i < nextLine.length; i++) {
                    if (i == numberOfColumns)
                        break;
                    result[i] = nextLine[i];                    
                }
                return result;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported.");
            }
        };
    }


}