/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zdenekdrahos.AI.NeuralNetwork;

import org.junit.Test;
import static org.junit.Assert.*;
import zdenekdrahos.AI.ActivationFunctions.ActivationFactory;
import zdenekdrahos.AI.ActivationFunctions.IActivationFactory;
import zdenekdrahos.AI.NetworkTestUtil;
import zdenekdrahos.AI.NeuralNetwork.Layers.ILayer;
import zdenekdrahos.AI.NeuralNetwork.Layers.Layer;

public class INeuralNetworkTest {
    
    private INeuralNetwork network = new NeuralNetwork();
    private IActivationFactory factory = new ActivationFactory();

    @Test
    public void testAddLayer() {
        ILayer layer = new Layer(1, factory.getLinearFunction());
        assertEquals(0, network.getLayersCount());
        network.addLayer(layer);        
        assertEquals(1, network.getLayersCount());
        network.addLayer(layer);
        assertEquals(2, network.getLayersCount());
    }

    @Test
    public void testSetWeights() {        
        network = NetworkTestUtil.getNetwork();
        NetworkTestUtil.assertNetworkWeights(network, NetworkTestUtil.getWeights());
    }    

}