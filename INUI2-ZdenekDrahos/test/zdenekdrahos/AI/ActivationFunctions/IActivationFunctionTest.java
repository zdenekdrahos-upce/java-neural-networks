
package zdenekdrahos.AI.ActivationFunctions;

import org.junit.Test;
import static org.junit.Assert.*;

public class IActivationFunctionTest {
    
    
    @Test
    public void testActivate() {
        // otherwise error because no tests in class
    }
    
    public void assertActivate(IActivationFunction function, double[] input, double[] expected) {
        for (int i = 0; i < input.length; i++) {            
            assertEquals(expected[i], function.activate(input[i]), 0.1);
        }
    }
}