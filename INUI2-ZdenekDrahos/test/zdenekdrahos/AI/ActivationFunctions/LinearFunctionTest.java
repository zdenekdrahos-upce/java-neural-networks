package zdenekdrahos.AI.ActivationFunctions;

import org.junit.Test;

public class LinearFunctionTest {

    private IActivationFunctionTest test = new IActivationFunctionTest();

    @Test
    public void testActivate() {
        double[] input = {Double.MIN_VALUE, -5, 0, 5, Double.MAX_VALUE};
        double[] expected = {Double.MIN_VALUE, -5, 0, 5, Double.MAX_VALUE};
        IActivationFunction func = new LinearFunction();
        test.assertActivate(func, input, expected);
    }
}