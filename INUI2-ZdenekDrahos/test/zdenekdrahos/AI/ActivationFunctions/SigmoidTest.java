
package zdenekdrahos.AI.ActivationFunctions;

import org.junit.Test;

public class SigmoidTest {

    private IActivationFunctionTest test = new IActivationFunctionTest();

    @Test
    public void testActivate() {
        double[] input = {Double.MIN_VALUE, -20, 0, 20, Double.MAX_VALUE};
        double[] expected = {0, 0, 0.5, 1, 1};
        IActivationFunction func = new Sigmoid();
        test.assertActivate(func, input, expected);
    }
}