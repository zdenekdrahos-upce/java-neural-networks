
package zdenekdrahos.AI.ActivationFunctions;

import org.junit.Test;

public class HyperbolicTangentTest {
    

    private IActivationFunctionTest test = new IActivationFunctionTest();

    @Test
    public void testActivate() {
        double[] input = {Double.MIN_VALUE, -20, 0, 20, Double.MAX_VALUE};
        double[] expected = {-1, -1, 0, 1, 1};
        IActivationFunction func = new HyperbolicTangent();
        test.assertActivate(func, input, expected);
    }
}