
package zdenekdrahos.AI.Kohonen;

import zdenekdrahos.AI.KohonenMap.IKohonenMap;
import zdenekdrahos.AI.KohonenMap.KohonenMap;
import java.io.FileNotFoundException;
import org.junit.Test;
import static org.junit.Assert.*;
import zdenekdrahos.Arrays.Converter2D.IStringToDouble;
import zdenekdrahos.Arrays.Converter2D.StringToDouble;
import zdenekdrahos.CsvImport.CsvImport;
import zdenekdrahos.CsvImport.ICsvImport;

public class IKohonenMapTest {

    
    private String filePath = "import-data/data.csv";
    private int iterationsCount = 500;
    private double alfa = 0.75;
    private int groupsCount = 2;
    private Double[][] expectedWeights = new Double[][] {
        {0.1828, 5.6345},
        {-0.1233, -1.8783}
    };
    
    @Test
    public void testTrain() throws FileNotFoundException {
        IKohonenMap map = new KohonenMap();
        map.setData(getExampleInput());
        map.train(alfa, groupsCount, iterationsCount);
        Double[][] weights = map.getWeights();
        for (int i = 0; i < weights.length; i++) {
            for (int j = 0; j < weights[0].length; j++) {
                assertTrue(weights[i][j] - expectedWeights[i][j] < 0.1);
            }
        }
    }

    private Double[][] getExampleInput() throws FileNotFoundException {
        ICsvImport csvImport = new CsvImport();
        IStringToDouble builder = new StringToDouble();
        csvImport.importFile(filePath, 2, ';');
        return builder.parseStringArray(csvImport.iterator());
    }

}