package zdenekdrahos.AI.FeedForward;

import org.junit.Test;
import zdenekdrahos.AI.NetworkTestUtil;
import zdenekdrahos.AI.NeuralNetwork.INeuralNetwork;

public class IFeedForwardTest {

    private INeuralNetwork network;
    private IFeedForward feedForward = new FeedForward();

    public IFeedForwardTest() {
        network = NetworkTestUtil.getNetwork();
    }

    @Test
    public void testBuildNetwork() {
        double[] input = {0};
        double[][] expectedValues = {
            { 0.099668,  -0.197375,  0.148885,  -0.049958  },
            { 0.127635  }
        };

        INetworkValues values = feedForward.buildNetwork(network, input);
        NetworkTestUtil.assertNetworkValues(network, values, expectedValues);
    }

}