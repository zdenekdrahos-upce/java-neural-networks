# JAVA Neural Networks

## About

Project developed as semestral project in [Introduction to Artificial Intelligence](http://ects.upce.cz/predmet/KRP/INUI2?lang=en&rocnik=1) at [University of Pardubice](http://www.upce.cz/en/index.html).

* **Kohonen map** - self organized map for data classification.
* **Feed Forward neural network**
* **Back Propagation** - training algorithm for feedforward neural networks.

## GUI application

You can import your data in *CSV* format to application. The data can be used for classification into selected number of groups with Kohonen Map and you can display graph for 2D data. Or you can approximate data with a neural network, which will be trained with Back Propagation algorithm. 

![Import CSV example](http://zdenekdrahos.bitbucket.org/java-neural-networks/import-csv-example.png "Import CSV example")
![Kohonen map example](http://zdenekdrahos.bitbucket.org/java-neural-networks/kohonen-map-example.png "Kohonen map example")
![Back propagation exampl](http://zdenekdrahos.bitbucket.org/java-neural-networks/back-propagation-example.png "Back propagation example")

## Example code

### Classifying data with Kohonen map

    Double[][] inputData = new Double[][]{
        {1.0, 2.0, ...}, 
        {2.0, 5.5, ...},
        ...
    };
    double learningRate = 0.75;
    int groupsCount = 2;
    int iterationsCount = 500;
    
    IKohonenMap kohonenMap = new KohonenMap();
    kohonenMap.setData(inputData);
    kohonenMap.train(learningRate, numberOfGroups, numberOfIterations);
    kohonenMap.classifyData();
    kohonenMap.sortClassifiedData();
    List<List<Double[]>> result = kohonenMap.getClassifiedData();

### Training feedforward neural network

    // Build feedforward neural network
    int[] topology = {2, 2, 1};
    Activations[] activations = {Activations.LIN, Activations.SIG,
                             Activations.LIN};
    INetworkBuilder networkBuilder = new NetworkBuilder();
    INeuralNetwork network = networkBuilder.build(topology, activations);
    
    // Input
    TrainingInput input = new TrainingInput();
    input.iterationsCount = 1000;
    input.learningRate = 0.2;
    input.momentum = 0;
    input.inputs = new double[][] { {0, 0}, ... };
    input.targets = new double[][] { {1}, ... };
    
    // Output 
    double errorToleration = 0.0005;
    TrainingOutput output = new TrainingOutput(errorToleration);
    
    // Init training - sets training algorithm, data separator, etc.
    ITrainingBuilder trainBuilder = new TrainingBuilder();
    ITraining training = trainBuilder.build();
    
    // Train network and simulate data with network
    training.train(network, input, output);
    double[] inputPattern = {0,0};
    train.simulate(inputPattern);

## Used libraries

* [OpenCSV 2.3](http://opencsv.sourceforge.net/) - csv parser library for Java
* [JFreeChart 1.0.14](http://www.jfree.org/jfreechart/) - Java chart library
    
## Facts

License: [New BSD License](https://bitbucket.org/zdenekdrahos/java-neural-networks/src/tip/INUI2-ZdenekDrahos/src/license.txt).

Author: [Zdenek Drahos](https://bitbucket.org/zdenekdrahos).
